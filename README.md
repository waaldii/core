**Opis**

Polska wersja Foundry VTT. Moduł ten zapewnia tłumaczenie interfejsu aplikacji (przynajmniej tych, które zostały udostępnione przez developerów). 

**Instalacja**
1. Skopiować adres moduje.json i wkleić go w pole Manifest URL w aplikacji
(link: https://gitlab.com/fvtt-poland/core/-/raw/main/pl-PL/module.json?inline=false)
2. Zainstalować Moduł
3. Wybrać język polski z ustawień systemu (dzięki temu mody, które zapewniają wsparcie dla języka polskiego zostaną również przetłumaczone.)

**Informacje dodatkowe**

Znalazłeś buga, masz sugestię lub pytania? Napisz mi wiadomość bezpośrednio na waaldii@outlook.com
